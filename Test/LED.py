import sys
import RPi.GPIO as GPIO
import time
pin = int(sys.argv[1])
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(pin,GPIO.OUT)

for x in range(1,10):
	print pin , " ON"
	GPIO.output(pin,GPIO.HIGH)
	time.sleep(1)
	print pin , " OFF"
	GPIO.output(pin,GPIO.LOW)
	sleep(1)
