﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace EvE.Models
{
    public class LarmResponse
    {
        private static LarmResponse instance;

        private bool newResponse;

        public void ClearResponse()
        {
            this.newResponse = false;
        }

        public bool IsNewResponse()
        {
            return this.newResponse;
        }

        public static LarmResponse Instance
        {
            get
            {
                if (null == instance)
                {
                    instance = new LarmResponse();
                }

                return instance;
            }
        }

        public List<string> Responses { get; set; }

        public LarmResponse()
        {
            this.Responses = new List<string>();
        }

        public void AddResponse(string msg)
        {
            this.Responses.Add(msg);
            this.newResponse = true;
        }

        public string LastResponse
        {
            get
            {
                if (this.Responses == null || this.Responses.Count == 0)
                {
                    return null;
                }

                return this.Responses.Last();
            }
        }
    }
}