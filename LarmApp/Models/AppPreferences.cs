﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Preferences;

namespace EvE.Models
{
    public class AppPreferences
    {
        private ISharedPreferences mSharedPrefs;
        private ISharedPreferencesEditor mPrefsEditor;
        private Context mContext;

        private static string PREFERENCE_LARM_PHONENUMBER = "PREFERENCE_LARM_PHONENUMBER";
        private static string PREFERENCE_LARM_PASSWORD = "PREFERENCE_LARM_PASSWORD";
        private static string PREFERENCE_CONTROL_PASSWORD = "PREFERENCE_CONTROL_PASSWORD";
        private static string PREFERENCE_TELLDUS_HOST = "PREFERENCE_TELLDUS_HOST";
        private static string PREFERENCE_VPN_CLIENT_NAME = "PREFERENCE_VPN_CLIENT_NAME";
        private static string PREFERENCE_TELLDUS_API_KEY = "PREFERENCE_TELLDUS_API_KEY";

        public string LarmPhoneNumber { get; set; }
        public string LarmRemotePassword { get; set; }
        public string LarmControlPassword { get; set; }
        public string TelldusHost { get; set; }
        public string TelldusApiKey { get; set; }
        public string VPNClientName { get; set; }

        private static AppPreferences instance;

        public static AppPreferences Instance
        {
            get
            {
                if(null == instance)
                {
                    instance = new AppPreferences(Application.Context);
                }

                return instance;
            }
        }

        public AppPreferences(Context context)
        {
            this.mContext = context;
            mSharedPrefs = PreferenceManager.GetDefaultSharedPreferences(mContext);
            this.Load();
        }

        public void Save()
        {
            mPrefsEditor = mSharedPrefs.Edit();
            mPrefsEditor.PutString(PREFERENCE_LARM_PHONENUMBER, this.LarmPhoneNumber);
            mPrefsEditor.PutString(PREFERENCE_LARM_PASSWORD, this.LarmRemotePassword);
            mPrefsEditor.PutString(PREFERENCE_CONTROL_PASSWORD, this.LarmControlPassword);
            mPrefsEditor.PutString(PREFERENCE_TELLDUS_HOST, this.TelldusHost);
            mPrefsEditor.PutString(PREFERENCE_VPN_CLIENT_NAME, this.VPNClientName);
            mPrefsEditor.PutString(PREFERENCE_TELLDUS_API_KEY, this.TelldusApiKey);
            
            mPrefsEditor.Commit();
        }

        public void Load()
        {
            this.LarmRemotePassword = mSharedPrefs.GetString(PREFERENCE_LARM_PASSWORD, string.Empty);
            this.LarmControlPassword = mSharedPrefs.GetString(PREFERENCE_CONTROL_PASSWORD, string.Empty);
            this.LarmPhoneNumber = mSharedPrefs.GetString(PREFERENCE_LARM_PHONENUMBER, string.Empty);
            this.TelldusHost = mSharedPrefs.GetString(PREFERENCE_TELLDUS_HOST, string.Empty);
            this.VPNClientName = mSharedPrefs.GetString(PREFERENCE_VPN_CLIENT_NAME, string.Empty);
            this.TelldusApiKey = mSharedPrefs.GetString(PREFERENCE_TELLDUS_API_KEY, string.Empty);
        }
    }
}