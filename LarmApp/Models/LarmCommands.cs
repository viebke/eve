﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace EvE.Models
{
    public class LarmCommands
    {
        private static LarmCommands instance;

        public static LarmCommands Instance
        {
            get
            {
                if (null == instance)
                {
                    instance = new LarmCommands();
                }
                return instance;
            }
        }

        private Dictionary<string, LarmCommand> commands;

        public string RemotePassword { get; set; }

        public LarmCommands()
        {
            this.commands = new Dictionary<string, LarmCommand>();
            BuildCommands();
        }

        private void BuildCommands()
        {
            // Remote
            AddCommand("1", "Arm", string.Empty, false, false, true);
            AddCommand("2", "Disarm", string.Empty, false, false, true);
            AddCommand("100", "System status", string.Empty, false, false, true);
            AddCommand("102", "Alarm phone number", string.Empty, false, false, true);
            AddCommand("103", "Alarm SMS number", string.Empty, false, false, true);

            // Control
            AddCommand("00", "Language setting", "X: 0: English(default), 1: Chinese", true, false);
            AddCommand("01", "Voice Volume", "X: 1~3; 1: Low, 2: Medium, 3: High. System default: Medium", true, false);
            AddCommand("02", "Panel power failure/recovery/battery low power alert", "0: off, 1: on. System default: off", true, false);
            AddCommand("03", "SMS reply for setting", "0: off, 1: on. System default on", true, false);
            AddCommand("04", "SMS alert for RFID card disarm", "0: off, 1: on. System default off.", true, false);
            AddCommand("05", "Time setting", "XXXX: time 0000~2359", true, false);
            AddCommand("06", "Voice message timely alert", "X: 1~3, Y: 0/1", true, true);
            AddCommand("07", "Voice message alert time", "X: 1~3, XXXX: time 0000~2359", true, false);
            AddCommand("08", "Phone call time setting", "XX: time 00~60 minutes.", true, false);
            AddCommand("09", "Tamper alarm setting", "0: off, 1: on. System default: off.", true, false);
            AddCommand("10", "Wireless defense zone type setting", "XX: defense zone number 01~99. Y: zone type, 1. Real time(default)," +
                "2. Urgent, 3. Delay, 4. Close", true, true);
            AddCommand("11", "Wirelesee zone 'home arm' setting", "XX: defense zone number 01~99. Y: 0 off, 1 on.", true, true);
            AddCommand("12", "Wireless zone alarmring", "XX: defense zne number 01~99, Y:0 off, 1 on (default).", true, true);
            AddCommand("13", "Wired defense zone type setting", "XX: defense zone number 01~02, Y: zone type, 1. Real time (default), " +
                "2. Urgent, 3. Delay, 4. Close.", true, true);
            AddCommand("14", "Wired zone 'home arm' setting", "XX: defense zone number 01~99. Y: 0 off, 1 on.", true, true);
            AddCommand("15", "Wired zone alarm ring", "XX: defense zone number 01~02. Y: 0 off, 1 on", true, true);
            AddCommand("16", "Wired zone trigger method", "XX: defense zone number 01~02. Y: 1 no, 0 yes.", true, true);
            AddCommand("17", "Delete remote controller", string.Empty, false, false);
            AddCommand("18", "Delete wireless sensor", string.Empty, false, false);
            AddCommand("19", "Delete RFID card", string.Empty, false, false);
            AddCommand("30", "Change remote control password", "XXXXXX: new password (6 digit). System default 123456", true, false);
            AddCommand("31", "Change setting password", "XXXXXX: new password (6 digit). System default 123123." +
                "Remote control password and setting password cannot be the same", true, false);
            AddCommand("40", "Alarm call circle time", "XX: 00~99, system default: 03.", true, false);
            AddCommand("41", "Alarming time of siren", "XX: 00~30 minutes. 0 means no sound when alarming. System default: 03.", true, false);
            AddCommand("42", "Delay arm time", "XX: 00~99 seconds. System default 0 (no delay).", true, false);
            AddCommand("43", "Delay alarm time", "XX: 00~99 seconds. System default: 40", true, false);
            AddCommand("44", "Wireless transmission", "0: off, 1: on. System default: off", true, false);
            AddCommand("45", "Wireless siren encryption", "XXXX: 0000~9999. System default: 1111", true, false);
            AddCommand("50", "Store alarm phone number", "X: serial number 1~5. Y: phone number (max 20 digits)", true, true);
            AddCommand("51", "Alarm SMS number", "X: serial number 1~5. Y: phone number (maxx 20 digits)", true, true);
            AddCommand("52", "Delete SMS number", "X: serial number 1~5", true, false);
            AddCommand("90", "Wired zone alarm SMS content", "XX: zone number 01~02. YY: Content", true, true);
            AddCommand("91", "Wireless zone alarm SMS content", "XX: zone number 01~16. YY: Content", true, true);
            AddCommand("92", "SMS content for RFID card disarm", "XX: RFID card serial number 01~10.", true, false);
        }

        private void AddCommand(string code, string name, string desc, bool? xx = null, bool? yy = null, bool remotepass = false)
        {
            commands.Add(code, new LarmCommand(
                remotepass ? AppPreferences.Instance.LarmRemotePassword : AppPreferences.Instance.LarmControlPassword,
                code,
                name,
                desc,
                xx,
                yy));
        }

        public LarmCommand GetByCode(string code)
        {
            LarmCommand cmd;
            if (this.commands.TryGetValue(code, out cmd))
            {
                return cmd;
            }

            return null;
        }

        public LarmCommand[] All
        {
            get
            {
                return this.commands.Values.ToArray();
            }
        }
    }

    public class LarmCommand
    {
        public string Password { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public bool? XXPresent { get; set; }
        public bool? YYPresent { get; set; }

        public string Display
        {
            get
            {
                return string.Format("{0}-{1}", this.Code, this.Description);
            }
        }

        private string command;

        public LarmCommand(string pass, string code, string name, string desc, bool? xx = null, bool? yy = null)
        {
            this.Name = name;
            this.Password = pass;
            this.Code = code;
            this.XXPresent = xx;
            this.YYPresent = yy;
        }

        public string GenerateCommand(string xx, string yy)
        {
            try
            {
                StringBuilder cmd = new StringBuilder();
                cmd.Append(this.Password);
                cmd.Append("#");
                cmd.Append(this.Code);

                if (!string.IsNullOrEmpty(xx))
                {
                    cmd.Append("*");
                    cmd.Append(xx);
                }

                if (!string.IsNullOrEmpty(yy))
                {
                    cmd.Append("*");
                    cmd.Append(yy);
                }

                cmd.Append("#");
                command = cmd.ToString();
                return command;
            }
            catch (Exception ex)
            {
                //TODO - log 
                throw;
            }
        }

        public string GenerateCommand(string xx)
        {
            return this.GenerateCommand(xx);
        }

        public override string ToString()
        {
            return command;
        }
    }
}