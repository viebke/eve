﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Content;
using System;

namespace EvE
{
    [Activity(Label = "EvE", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        private const string TAG = "MainActivity";

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView (Resource.Layout.Main);

            Button larm = this.FindViewById<Button>(Resource.Id.button_firstpage_larm);
            Button camera = this.FindViewById<Button>(Resource.Id.button_firstpage_camera);
            Button smarthome = this.FindViewById<Button>(Resource.Id.button_firstpage_smarthome);
            Button prefs = this.FindViewById<Button>(Resource.Id.button_firstpage_preferences);

            larm.Click += Larm_Click;
            camera.Click += Camera_Click;
            smarthome.Click += Smarthome_Click;
            smarthome.LongClick += Smarthome_LongClick;
            prefs.Click += Prefs_Click;
        }

        private void Smarthome_LongClick(object sender, Android.Views.View.LongClickEventArgs e)
        {
            try
            {
                Intent intent = this.PackageManager.GetLaunchIntentForPackage("com.telldus.live.mobile");
                StartActivity(intent);
            }
            catch (Exception ex)
            {
                Android.Util.Log.Debug(TAG, "Error starting Camera: " + ex.Message);
            }
        }

        private void Prefs_Click(object sender, System.EventArgs e)
        {
            StartActivity(typeof(Activities.PreferenceActivity));
        }

        private void Smarthome_Click(object sender, System.EventArgs e)
        {
            //StartActivity(typeof(Activities.SmartHomeActivity));

            try
            {
                Intent intent = this.PackageManager.GetLaunchIntentForPackage("com.imperihome.lite");
                StartActivity(intent);
            }
            catch
            {
                try
                {
                    Intent intent = this.PackageManager.GetLaunchIntentForPackage("com.telldus.live.mobile");
                    StartActivity(intent);
                }
                catch (Exception ex)
                {
                    Android.Util.Log.Debug(TAG, "Error starting Camera: " + ex.Message);
                }
            }
        }

        private void Camera_Click(object sender, System.EventArgs e)
        {
            //StartActivity(typeof(Activities.CameraActivity));
            try
            {
                Intent intent = new Intent(Intent.ActionMain);
                intent.SetComponent(new ComponentName("com.alexvas.dvr", "com.alexvas.dvr.MainActivity"));
                StartActivity(intent);
            }
            catch (Exception ex)
            {
                Android.Util.Log.Debug(TAG, "Error starting SmartHome: " + ex.Message);
            }
        }

        private void Larm_Click(object sender, System.EventArgs e)
        {
            StartActivity(typeof(Activities.LarmActivity));
        }
    }
}

