﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using EvE.Models;

namespace EvE.Activities
{
    [Activity(Label = "Inställningar")]
    public class PreferenceActivity : Activity
    {
        private const string TAG = "PreferenceActivity";
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            try
            {
                this.SetContentView(Resource.Layout.Preferences);

                EditText larmphone = this.FindViewById<EditText>(Resource.Id.preference_larmnumber);
                EditText larmremotepassword = this.FindViewById<EditText>(Resource.Id.preference_larmremotepassword);
                EditText larmcontrolpassword = this.FindViewById<EditText>(Resource.Id.preference_larmcontrolpassword);
                EditText telldusremotehost = this.FindViewById<EditText>(Resource.Id.preference_telldushost);
                EditText telldusapikey = this.FindViewById<EditText>(Resource.Id.preference_telldusapikey);
                EditText vpnclient = this.FindViewById<EditText>(Resource.Id.preference_vpnclient);
                Button save = this.FindViewById<Button>(Resource.Id.preference_save);

                larmphone.Text = AppPreferences.Instance.LarmPhoneNumber;
                larmremotepassword.Text = AppPreferences.Instance.LarmRemotePassword;
                larmcontrolpassword.Text = AppPreferences.Instance.LarmControlPassword;
                telldusremotehost.Text = AppPreferences.Instance.TelldusHost;
                telldusapikey.Text = AppPreferences.Instance.TelldusApiKey;
                vpnclient.Text = AppPreferences.Instance.VPNClientName;


                save.Click += (sender, e) =>
                {
                    AppPreferences.Instance.LarmPhoneNumber = larmphone.Text;
                    AppPreferences.Instance.LarmRemotePassword = larmremotepassword.Text;
                    AppPreferences.Instance.LarmControlPassword = larmcontrolpassword.Text;
                    AppPreferences.Instance.TelldusHost = telldusremotehost.Text;
                    AppPreferences.Instance.TelldusApiKey = telldusapikey.Text;
                    AppPreferences.Instance.VPNClientName = vpnclient.Text;
                    AppPreferences.Instance.Save();                    
                };
            }
            catch (Exception ex)
            {
                Android.Util.Log.Debug(TAG, "Error initiating PreferenceActivity: " + ex.Message);
            }
        }
    }
}