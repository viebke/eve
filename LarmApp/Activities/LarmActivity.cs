﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using EvE.Models;

namespace EvE.Activities
{
    [Activity(Label = "Larm")]
    public class LarmActivity : Activity
    {
        private const string TAG = "LarmActivity";

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            try
            {
                this.SetContentView(Resource.Layout.Larm);
                Adapters.LarmCommandsAdapter adapter = new Adapters.LarmCommandsAdapter(this, Models.LarmCommands.Instance.All
                    .Where(x => x.Code != "1" && x.Code != "2").ToArray());
                ListView v = this.FindViewById<ListView>(Resource.Id.commandslist);
                v.Adapter = adapter;

                TextView larmon = this.FindViewById<TextView>(Resource.Id.larmon);
                TextView larmoff = this.FindViewById<TextView>(Resource.Id.larmoff);
                larmon.Click += Larmon_Click;
                larmoff.Click += Larmoff_Click;
            }
            catch (Exception ex)
            {
                Android.Util.Log.Debug(TAG, "Error initiating LarmActivity: " + ex.Message);
            }
        }

        private void Larmoff_Click(object sender, EventArgs e)
        {
            try
            {
                Controllers.LarmController.LarmOff();
                Controllers.TelldusController.CameraOff();
            }
            catch (Exception ex)
            {
                Android.Util.Log.Debug(TAG, "Unable to larm off: " + ex.Message);
            }
        }

        private void Larmon_Click(object sender, EventArgs e)
        {
            try
            {
                Controllers.LarmController.LarmOn();
                Controllers.TelldusController.CameraOn();
            }
            catch (Exception ex)
            {
                Android.Util.Log.Debug(TAG, "Unable to larm on: " + ex.Message);
            }
        }



    }
}