﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Net;
using Android.Net.Wifi;

namespace EvE.Helpers
{
    public class NetworkHelper
    {
        public static bool ConnectedToHomeWifi()
        {
            try
            {
                WifiManager mgr = (WifiManager)Application.Context.GetSystemService(Android.App.Application.WifiService);
                
                if (mgr.WifiState == WifiState.Enabled)
                { 
                    if (!mgr.ConnectionInfo.SSID.ToUpper().Contains("kabelfritt".ToUpper()))
                    {
                        return false;
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
    }
}