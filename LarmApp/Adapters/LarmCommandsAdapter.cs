﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using EvE.Models;

namespace EvE.Adapters
{
    public class LarmCommandsAdapter : BaseAdapter
    {
        private const string TAG = "LarmCommandsAdapter";

        private LarmCommand[] data;

        Context context;

        public LarmCommandsAdapter(Context context, LarmCommand[] items)
        {
            this.context = context;
            this.data = items;
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return position;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView;

            try
            {
                LarmCommandsAdapterViewHolder holder = null;

                LarmCommand data = this.data[position];

                holder = new LarmCommandsAdapterViewHolder();
                var inflater = context.GetSystemService(Context.LayoutInflaterService).JavaCast<LayoutInflater>();
                view = inflater.Inflate(Resource.Layout.LarmCommandViewItem, parent, false);
                holder.Code = data.Code;
                holder.Description = data.Description;
                holder.Name = data.Name;
                view.Tag = holder;
                holder.Position = position;

                view.FindViewById<TextView>(Resource.Id.Name).Text = holder.Name;
                view.FindViewById<TextView>(Resource.Id.Description).Text = holder.Description;
                view.FindViewById<TextView>(Resource.Id.Code).Text = holder.Code;

                view.Click += View_Click;
            }
            catch (Exception ex)
            {
                Android.Util.Log.Debug(TAG, "Error generating view: " + ex.Message);
            }

            return view;
        }

        private void View_Click(object sender, EventArgs e)
        {
            try
            {
                var d = sender as View;
                var dd = d.Tag as LarmCommandsAdapterViewHolder;
                var pos = dd.Position;
                this.LastPosition = pos;

                var inflater = context.GetSystemService(Context.LayoutInflaterService).JavaCast<LayoutInflater>();
                var inputView = inflater.Inflate(Resource.Layout.LarmSendCommandInputView, null);
                Android.App.AlertDialog.Builder dialog = new AlertDialog.Builder(this.context);
                dialog.SetTitle("Skicka larmkommando");
                dialog.SetView(inputView);

                var command = this.data[pos];

                EditText xx = inputView.FindViewById<EditText>(Resource.Id.larmsendcommandxx);
                EditText yy = inputView.FindViewById<EditText>(Resource.Id.larmsendcommandyy);
                if (!command.XXPresent.GetValueOrDefault(false))
                {
                    xx.Visibility = ViewStates.Gone;
                }
                else
                {
                    xx.Visibility = ViewStates.Visible;
                }
                if (!command.YYPresent.GetValueOrDefault(false))
                {
                    yy.Visibility = ViewStates.Gone;
                }
                else
                {
                    yy.Visibility = ViewStates.Visible;
                }

                TextView name = inputView.FindViewById<TextView>(Resource.Id.larmsendcommandname);
                TextView desc = inputView.FindViewById<TextView>(Resource.Id.larmsendcommanddesc);
                name.Text = command.Name;
                desc.Text = command.Description;
                dialog.SetPositiveButton("Skicka", SendClicked);
                dialog.SetNegativeButton("Avbryt", delegate { dialog.Dispose(); });
                dialog.Show();
            }
            catch (Exception ex)
            {
                Android.Util.Log.Debug(TAG, "Unable to create dialog: " + ex.Message);
                Toast.MakeText(this.context, "Unable to create dialog", ToastLength.Long).Show();
            }
        }

        private void SendClicked(object sender, DialogClickEventArgs e)
        {
            try
            {
                // Read input
                var dialog = (Android.App.AlertDialog)sender;
                EditText xx = dialog.FindViewById<EditText>(Resource.Id.larmsendcommandxx);
                EditText yy = dialog.FindViewById<EditText>(Resource.Id.larmsendcommandyy);

                string xxtext = xx.Text;
                string yytext = yy.Text;

                var command = this.data[this.LastPosition];
                string cmd = command.GenerateCommand(xxtext, yytext);
                Controllers.CommandController.SendLarmCommand(cmd);
            }
            catch (Exception ex)
            {
                Android.Util.Log.Debug(TAG, "Unable to send sms command: " + ex.Message);
                Toast.MakeText(this.context, "Unable to send sms command: " + ex.Message, ToastLength.Long).Show();
            }
        }

        //Fill in cound here, currently 0
        public override int Count
        {
            get
            {
                return data.Length;
            }
        }

        public int LastPosition { get; private set; }
    }

    class LarmCommandsAdapterViewHolder : Java.Lang.Object
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public int Position { get; internal set; }
    }
}