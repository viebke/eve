﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net.Http;

namespace EvE.Controllers
{
    public class HttpController
    {
        public static void Post(string url, string json)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(url);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync("/command", content).Result;
                var result = response.Content.ReadAsStringAsync().Result;
            }
            catch (Exception ex)
            {
                Android.Util.Log.Debug(nameof(HttpController), "Unable to send post request: " + ex.Message);
                throw;
            }
        }
    }
}