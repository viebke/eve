﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using EvE.Models;
using System.Threading.Tasks;
using Android.Telephony;

namespace EvE.Controllers
{
    public class SMSController
    {
        private const string TAG = "SMSController";

        public static void SendSMS(string cmd)
        {
            string phonenumber = AppPreferences.Instance.LarmPhoneNumber;
            Android.Util.Log.Debug(TAG, string.Format("Sending sms command {0} to {1}", cmd, phonenumber));
            SendSMS(phonenumber, cmd);
        }

        private static void SendSMS(String phoneNumber, String message)
        {
            Toast.MakeText(Application.Context, string.Format("Skickar {0} => {1}", message, phoneNumber), ToastLength.Long).Show();
            SmsManager sms = SmsManager.Default;            
            sms.SendTextMessage(phoneNumber, null, message, null, null);
        }
    }
}