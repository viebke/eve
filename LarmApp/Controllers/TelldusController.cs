﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using EvE.Models;

namespace EvE.Controllers
{
    class TelldusController
    {
        private static string TAG = "TelldusController";

        public static void CameraOff()
        {
            bool vpnstarted = false;
            try
            {
                if (!Helpers.NetworkHelper.ConnectedToHomeWifi())
                {
                    try
                    {
                        VPNController.StartVPN();
#warning make sure we are connected here 
                        vpnstarted = true;
                    }
                    catch (Exception ex)
                    {
                        Android.Util.Log.Debug(TAG, "Unable to connect to vpn: " + ex.Message);
                        throw;
                    }
                }
            }
            catch (Exception ex)
            {
                Android.Util.Log.Debug(TAG, "Unable to check wifi connectivity: " + ex.Message);
                throw new Exception("Unable to check wifi connectivity: " + ex.Message, ex);
            }
            
            try
            {
#warning generate json and get ip from settings
                string jsonData = @"{""api_key"" : ""gUX8pr?p4atU"", ""type"" : ""telldusdevice"", ""action"" : ""off"", ""signal"" : ""1845578""}";
                Controllers.HttpController.Post(AppPreferences.Instance.TelldusHost + "/command", jsonData);
                Toast.MakeText(Application.Context, "Stängde kamera via telldus", ToastLength.Long).Show();
            }
            catch (Exception ex)
            {
                Android.Util.Log.Debug(TAG, "Error sending telldus command: " + ex.Message);
                Toast.MakeText(Application.Context, "Misslyckades med att stänga kamera via telldus", ToastLength.Long).Show();
            }

            if (vpnstarted)
            {
                try
                {
                    VPNController.StopVPN();
                }
                catch (Exception ex)
                {
                    Android.Util.Log.Debug(TAG, "Unable to stop VPN: " + ex.Message);
                    throw new Exception("Unable to stop VPN: " + ex.Message, ex);
                }
            }
        }

        public static void CameraOn()
        {
            bool vpnstarted = false;
            try
            {
                if (!Helpers.NetworkHelper.ConnectedToHomeWifi())
                {
                    try
                    {
                        VPNController.StartVPN();
#warning make sure we are connected here 
                        vpnstarted = true;
                    }
                    catch (Exception ex)
                    {
                        Android.Util.Log.Debug(TAG, "Unable to connect to vpn: " + ex.Message);
                        throw;
                    }
                }
            }
            catch (Exception ex)
            {
                Android.Util.Log.Debug(TAG, "Unable to check wifi connectivity: " + ex.Message);
                throw new Exception("Unable to check wifi connectivity: " + ex.Message, ex);
            }

            try
            {
#warning generate json and get ip from settings
                string jsonData = @"{""api_key"" : ""gUX8pr?p4atU"", ""type"" : ""telldusdevice"", ""action"" : ""on"", ""signal"" : ""1845578""}";
                Controllers.HttpController.Post(AppPreferences.Instance.TelldusHost + "/command", jsonData);
                Toast.MakeText(Application.Context, "Startade kamera via telldus", ToastLength.Long).Show();
            }
            catch (Exception ex)
            {
                Android.Util.Log.Debug(TAG, "Error sending telldus command: " + ex.Message);
                Toast.MakeText(Application.Context, "Misslyckades med att starta kamera via telldus", ToastLength.Long).Show();
                throw new Exception("Error sending telldus command: " + ex.Message, ex);
            }

            if (vpnstarted)
            {
                try
                {
                    VPNController.StopVPN();
                }
                catch (Exception ex)
                {
                    Android.Util.Log.Debug(TAG, "Unable to stop VPN: " + ex.Message);
                    throw new Exception("Unable to stop VPN: " + ex.Message, ex);
                }
            }
        }

    }
}