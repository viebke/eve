﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace EvE.Controllers
{
    public class VPNController
    {
        private static string TAG = "VPNController";

        public static void StartVPN()
        {
            try
            {
                Intent openVPN = new Intent("android.intent.action.VIEW");
                openVPN.AddFlags(ActivityFlags.NewTask);
                openVPN.SetPackage("net.openvpn.openvpn");
                openVPN.SetClassName("net.openvpn.openvpn", "net.openvpn.openvpn.OpenVPNClient");
                openVPN.PutExtra("net.openvpn.openvpn.AUTOSTART_PROFILE_NAME", "viebke.hopto.org");
                Android.App.Application.Context.StartActivity(openVPN);
#warning want to close the activity as well
                System.Threading.Thread.Sleep(3000);
            }
            catch (Exception ex)
            {
                Android.Util.Log.Debug(TAG, "Unable to start VPN: " + ex.Message);
                throw;
            }
        }

        public static void StopVPN()
        {
            try
            {
                Intent openVPN = new Intent("android.intent.action.VIEW");
                openVPN.AddFlags(ActivityFlags.NewTask);
                openVPN.SetPackage("net.openvpn.openvpn");
                openVPN.SetClassName("net.openvpn.openvpn", "net.openvpn.openvpn.OpenVPNDisconnect");
                Android.App.Application.Context.StartActivity(openVPN);
            }
            catch (Exception ex)
            {
                Android.Util.Log.Debug(TAG, "Unable to start VPN: " + ex.Message);
                throw;
            }
        }
    }
}