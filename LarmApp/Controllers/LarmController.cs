﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace EvE.Controllers
{
    class LarmController
    {
        private static string TAG = "LarmController";

        public static void LarmOff()
        {
            try
            {
                var command = Models.LarmCommands.Instance.GetByCode("2");
                string cmd = command.GenerateCommand(string.Empty, string.Empty);
                Controllers.CommandController.SendLarmCommand(cmd);
                Toast.MakeText(Application.Context, "Slog av larm via SMS", ToastLength.Long).Show();
            }
            catch (Exception ex)
            {
#warning fallback todo
                Android.Util.Log.Debug(TAG, "Error sending larm off command: " + ex.Message);
                Toast.MakeText(Application.Context, "Misslyckades att slå av larm via SMS", ToastLength.Long).Show();
            }
        }

        public static void LarmOn()
        {
            try
            {
                var command = Models.LarmCommands.Instance.GetByCode("1");
                string cmd = command.GenerateCommand(string.Empty, string.Empty);
                Controllers.CommandController.SendLarmCommand(cmd);
                Toast.MakeText(Application.Context, "Slog på larm via SMS", ToastLength.Long).Show();
            }
            catch (Exception ex)
            {
                Android.Util.Log.Debug(TAG, "Error sending larm on command: " + ex.Message);
                Toast.MakeText(Application.Context, "Misslyckades med att slå på larm via SMS", ToastLength.Long).Show();
            }
        }
    }
}