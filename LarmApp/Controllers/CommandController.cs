﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using EvE.Models;

namespace EvE.Controllers
{
    public class CommandController
    {
        private const string TAG = "CommandController";

        public static void SendLarmCommand(LarmCommand cmd, string xx = null, string yy = null)
        {
            Android.Util.Log.Debug(TAG, "Sending command by sms");
            string command = cmd.GenerateCommand(xx, yy);
            Controllers.SMSController.SendSMS(command);
        }

        public static void SendLarmCommand(string cmd)
        {
            Android.Util.Log.Debug(TAG, "Sending command by sms plain");
            Controllers.SMSController.SendSMS(cmd);
        }

        public static string SendLarmCommandAndWait(LarmCommand cmd, string xx = null, string yy = null)
        {
            Android.Util.Log.Debug(TAG, "Sending command by sms, waiting for response");
            string command = cmd.GenerateCommand(xx, yy);
            Controllers.SMSController.SendSMS(command);

            for (int i = 0; i < 5; i++)
            {
                System.Threading.Thread.Sleep(3000);

                if (Models.LarmResponse.Instance.IsNewResponse())
                {
                    break;
                }
            }
            return Models.LarmResponse.Instance.LastResponse;
        }
    }
}