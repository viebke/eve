#!/usr/bin/python3.4
from app import app, models, forms
import getopt, sys, json, logging
from flask import render_template, request, jsonify, redirect, url_for, session, flash
from classes.defs import Defs

# takes care of the authentication parts
from flask_login import LoginManager, login_user, logout_user, login_required, current_user
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"

logging.basicConfig(filename='log.log', level=logging.DEBUG)

from app.controllers.commandcontroller import CommandController

@app.route('/')
@app.route('/index')
@login_required
def main():
    return render_template('index.html', title="Översikt", Defs=Defs)

@app.route('/camera')
@login_required
def camera():
    return render_template('camera.html', title='Kamera')

@app.route('/command', methods=['POST'])
def command():
    content = request.get_json(silent=False)

    try:
        CommandController.executecommand(content)
        return jsonify("OK")
    except Exception as ex:
        logging.debug("Unable to execute command: %s" % (str(ex)))
        return jsonify("Error: " + str(ex)), 500

@app.route('/devices', methods=['GET'])
@login_required
def devices():
    logging.info("Retrieving all devices started")
    devices = models.Device.query.all()
    logging.info("Retrieving all devices done")
    return render_template('devices.html', title='Enheter', devices=devices)

@app.route('/devices/action', methods=['POST'])
@login_required
def devices_action():
    try:
        content = request.get_json(silent=False)
        CommandController.executedeviceaction(content)
        return jsonify("OK")
    except Exception as ex:
        logging.debug("Unable to process request. %s" % (str(ex)))
        return jsonify("Error: " + str(ex)), 500

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = forms.UserLoginForm(request.form)
    error = None
    if request.method == 'POST':
        if form.validate():
            user = models.User.query.filter_by(name=form.username.data).first()
            if user:
                if login_user(user):
                    logging.debug('Inloggad användare %s', user.name)
                    return redirect(url_for('main'))
            error = 'Felaktiga inloggningsuppgifter.'
        else:
             flash('Valideringsfel, fyll i alla fält')
    return render_template('login.html', form=form, error=error)

@app.route("/logout")
@login_required
def logout():
    logout_user()
    return "<h2>Utloggad</h2>"

@login_manager.user_loader
def load_user(userid):
    return models.User.query.get(userid)

if __name__ == "__main__":
    p = 5000
    d = True
    try:
        options, remainder = getopt.getopt(sys.argv[1:], 'p:', ['port='])
        for opt, arg in options:
            if opt in ('-p'):
                print("arg:" + arg)
                p = int(arg.strip())  
                d = False
    except getopt.GetoptError as getex:
        logging.debug("Unable to get options %s" % (str(getex)))

    app.run(host='0.0.0.0', port=p, debug=d)
