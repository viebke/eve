from app.models import TelldusDevice, KjellDevice, Device
import logging
logging.basicConfig(filename='log.log', level=logging.DEBUG)
from flask import app

class CommandController(object):
    """controller handling commands"""
    
    @staticmethod
    def executedeviceaction(cmd):
        try:
            deviceid = cmd.get("id", None)
            logging.info("deviceid: %s" % (deviceid))
            if deviceid is None:
                raise LookupError("Deviceid (%s) needs to be present" % (deviceid))

            action = cmd.get("action", None)
            logging.info("action: %s" % (action))
            if action is None:
                raise LookupError("Action (%s) needs to be present" % (action))
                        
            if(deviceid == "-1"): # -1 == "ALLA"
                devices = Device.query.all()
                for device in devices:
                    logging.info("Performing action %s for device with id %s and signal %s" % (action, device.id, device.signal))
                    CommandController.performaction(action, device.type, device.signal)
                    logging.info("Action performed successfully!")
            else: #Get device from database                
                logging.info("performing query")
                device = Device.query.filter_by(id=deviceid).first()
                logging.info("query performed")

                if device is None:
                    raise Exception("device not found in db with id %s" % (deviceid))

                # Perform the action
                logging.info("Performing action %s for device with id %s and signal %s" % (action, deviceid, device.signal))
                CommandController.performaction(action, device.type, device.signal)
                logging.info("Action performed successfully!")

            #TODO - could be that we use the object methods instead of the static ones further on
        except Exception as ex:
            logging.debug("Unable to execute command: %s" % (str(ex)))
            raise

    @staticmethod
    def performaction(action, type, signal):
        if type == "telldusdevice":
            if action == "on":
                TelldusDevice.on(signal)
                logging.info("turned telldus device %s on" % (signal))
            elif action == "off":
                TelldusDevice.off(signal)
                logging.info("turned telldus device %s off" % (signal))
            else:
                raise NotImplementedError("action %s is not implemented for telldus device" % (action))
        elif type == "kjelldevice":
            if action == "on":
                KjellDevice.on(signal)
                logging.info("turned kjell device %s on" % (signal))
            elif action == "off":
                KjellDevice.off(signal)
                logging.info("turned kjell device %s off" % (signal))
            else:
                raise NotImplementedError("actions %s is not implemented for kjell device" % (action))
        else:
            raise NotImplementedError("Type %s is not implemented" % (type))

    @staticmethod
    def executecommand(cmd):
        try:
            api_key = cmd.get("api_key", None)
            logging.info("api_key: %s" % (api_key))
            if api_key != app.config['API_KEY']:
                raise KeyError("Invalid api key")       
            
            type = cmd.get("type", None)
            logging.info("type: %s" % (type))
            if type is None:
                raise LookupError("Type (%s) needs to be present" % (type))
            
            action = cmd.get("action", None)
            logging.info("action: %s" % (action))
            if action is None:
                raise LookupError("Action (%s) needs to be present" % (action))

            signal = cmd.get("signal", None)
            logging.info("signal: %s" % (signal))
            if signal is None:
                raise LookupError("Signal (%s) needs to be present" % (signal))
                       
            CommandController.performaction(action, type, signal)

        except Exception as ex:
            logging.debug("Unable to execute command: %s" % (str(ex)))
            raise


