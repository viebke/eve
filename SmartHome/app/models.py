#import sys
#sys.path.append('~/SmartHome/SmartHome/app/')
#sys.path.append('~/SmartHome/SmartHome/telldus/')
from app import db
import telldus.tdtool as telldus
import logging, subprocess
logging.basicConfig(filename='log.log', level=logging.DEBUG)

class Device(db.Model):
    """The base Device class"""   
    
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True, unique=False)
    signal = db.Column(db.String(120), index=True, unique=False)
    type = db.Column(db.String(120), index=True, unique=False)
    __table_args__ = {'extend_existing': True} 

    def __init__(self, id, name, type, signal):
        self.id = id
        self.name = name
        self.type = type
        self.signal = signal
        
    def __repr__(self):
        return '<Id %r><Name %r><Signal %r><Type %r>' % (self.id, self.name, self.signal, self.type)

    def __str__(self):
           return '<Id %s><Name %s><Signal %s><Type %s>' % (self.id, self.name, self.signal, self.type)


class KjellDevice(Device):
    """A Kjell device"""

    def __init__(self, id, name, signalid):
        Device.__init__(self, id, name, "KjellDevice")

    @staticmethod
    def on(signal):
        try:
            subprocess.call("sudo ~/rcswitch-pi/433Utils/RPi_utils/codesend " + str(signal), shell=True)
        except Exception as ex:
            logging.debug("Unable to send on signal to kjell device. %s" % (signal))
            raise
            
    @staticmethod
    def off(signal):
        try:
            subprocess.call("sudo ~/rcswitch-pi/433Utils/RPi_utils/codesend " + str(signal), shell=True)
        except Exception as ex:
            logging.debug("Unable to send off signal to kjell device. %s" % (signal))
            raise

class TelldusDevice(Device):
    """A Telldus device"""
    
    def __init__(self, houseid):
        Device.__init__(self, "-1", "N/A", "TelldusDevice")
        self.houseid = houseid
                        
    #def on(self):
    #    try:
    #       logging.debug("turning device %s on" % (self.houseid))
    #       telldus.doMethod(self.houseid, telldus.TELLSTICK_TURNON)
    #       logging.debug("turned device %s on" % (self.houseid))
    #    except Exception as ex:
    #       logging.debug("Unable to turn on telldus device: %s" % (str(ex))) 
    #       raise

    #def off(self):          
    #    try:
    #       logging.debug("turning device %s off" % (self.houseid))
    #       telldus.doMethod(self.houseid, telldus.TELLSTICK_TURNOFF)
    #       logging.debug("turned device %s off" % (self.houseid))
    #    except Exception as ex:
    #       logging.debug("Unable to turn off telldus device: %s" % (str(ex))) 
    #       raise
     
    @staticmethod 
    def on(houseid):
        try:
           logging.debug("turning device %s on" % (houseid))
           telldus.doMethod(houseid, telldus.TELLSTICK_TURNON)
           logging.debug("turned device %s on" % (houseid))
        except Exception as ex:
           logging.debug("Unable to turn on telldus device: %s" % (str(ex))) 
           raise

    @staticmethod
    def off(houseid):          
        try:
           logging.debug("turning device %s off" % (houseid))
           telldus.doMethod(houseid, telldus.TELLSTICK_TURNOFF)
           logging.debug("turned device %s off" % (houseid))
        except Exception as ex:
           logging.debug("Unable to turn off telldus device: %s" % (str(ex))) 
           raise

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True, unique=True)
    password = db.Column(db.String(120), index=True, unique=True)
    __table_args__ = {'extend_existing': True} 

    def __init__(self, name, password):
        self.name = name
        self.password = password

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        try:
            return unicode(self.id)  # python 2
        except NameError:
            return str(self.id)  # python 3

    def __repr__(self):
        return '<User %r>' % (self.nickname)

if __name__ == "__main__":
    device = TelldusDevice("TV", 1, 1668440)
    device.turnon()