import time
import http.server
import tdtool as telldus
import subprocess

HOST_NAME = '192.168.10.178'
PORT_NUMBER = 8081
SECRET = "D737BCF5C6C18AEC80D867ED1A093489"

def telldusDevice(x):
    return {
        'alla' :  1668481,
        'koket' : 1668472,
        'soffan' : 1668446,
        'tv' : 1668440,
        }.get(x.lower(), None)

def nativceDevice(x):
    return {
        'a' : ("16762196", "16762193"), 
        'b' : ("16762196", "16762193"), #change
        'c' : ("16762196", "16762193"), #change
        'd' : ("16762196", "16762193"), #change
        }.get(x.lower(), None)

def telldusAction(val):
    return {
        'on' : telldus.TELLSTICK_TURNON,
        'off' : telldus.TELLSTICK_TURNOFF,
        }.get(val.lower(), None)

def nativeAction(val):
    return {
        'on' : 0,
        'off' : 1,
        }.get(val.lower(), None)

def nativeCode(device, action):
    try:
        dev = nativceDevice(device)
        action = nativeAction(action)
        code = dev[action]
        return code
    except Exception as e:
        print(e)
        raise Exception("Unable to parse code")

#TODO - create enum
def deviceType(device):
    telldusdevice = telldusDevice(device)
    nativedevice = nativceDevice(device)
    if telldusdevice is not None:
        return 0
    elif nativedevice is not None:
        return 1
    else:
        return -1

def performAction(action, device):
    type = deviceType(device)
    
    if type == 0:
        try:
            telldusdevice = telldusDevice(device)
            telldusaction = telldusAction(action)
            if not telldusdevice is None and not telldusaction is None:
                telldus.doMethod(telldusdevice, telldusaction)
                return str("Performed telldus action: " + str(action) + " on device " + str(device))
            else:
                raise Exception("Device " + str(device) + " or action " + str(action) + " could not be retrieved")
        except Exception as e:
            print(e)
            raise Exception("Unable to perform telldus action")
    elif type == 1:
        try:
            native_code = nativeCode(device, action)
            if not native_code is None:
              subprocess.call("sudo ~/rcswitch-pi/433Utils/RPi_utils/codesend " + str(native_code), shell=True)
              return str("Performed native action: " + str(action) + " on device " + str(device))
            else:
             raise Exception("Device " + str(device) + " or action " + str(action) + " could not be retrieved")
        except Exception as e:
            print(e)
            raise Exception("Unable to perform native action")
    else:
        raise Exception("Unknonw type of device " + device)

class MyHandler(http.server.BaseHTTPRequestHandler):
    def do_HEAD(s):
      s.send_response(200)
      s.send_header("Content-type", "text/html")
      s.end_headers()

    def do_GET(s):  
        
        if s.path == "/favicon.ico":
            s.send_response(200)
            s.send_header("Content-type", "text/html")
            s.end_headers()
            return
        
        writeres = ""

        try:
                         
            query = s.path.replace("/?", "")
            query_components = [qc.split("=") for qc in query.split("&")]

            #Check secret
            secretitem = getOrDefault(query_components,"secret", None)
            if  secretitem is None or len(secretitem) != 2:
                print("Secret not provided")
                raise Exception("Secret not provided")
            if secretitem[1] != SECRET:
                print("Incorrect secret: " + secretitem[1])
                raise Exception("Incorrect secret")

            query_components.remove(secretitem)
                        
            for item in query_components:
                try:
                    res = performAction(item[0],item[1])
                    writeres = writeres + "</br>" + res
                except Exception as e:
                    writeres = writeres + "</br>" + str(e)
                 
            s.send_response(200)
        except Exception as e:
            writeres = writeres + "</br>" + str(e)
            s.send_response(500)
            
        s.send_header("Content-type", "text/html")
        s.end_headers()
        s.wfile.write(bytearray(writeres,"utf8"))
                        

def getOrDefault (l, idx, default):
    try:
        for item in l:
            if item[0] == idx:
                return item
    except IndexError:
        return default

if __name__ == '__main__':
    telldus.initconfig()
    #telldus.doMethod(1668481, telldus.TELLSTICK_TURNOFF)
    server_class = http.server.HTTPServer
    httpd = server_class((HOST_NAME, PORT_NUMBER), MyHandler)
    print(time.asctime(), "Server Starts - %s:%s" % (HOST_NAME, PORT_NUMBER))
    try:
       httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    print(time.asctime(), "Server Stops - %s:%s" % (HOST_NAME, PORT_NUMBER))