#!/usr/bin/python3.4
import unittest
from telldusdevice import TelldusDevice

class Test_testTelldusDevice(unittest.TestCase):
    def shouldTurnDeviceOn(self):
        print("shouldTurnDeviceOn")
        device = TelldusDevice("TV", 1, 1668440)
        device.turnon()
        # ok if no exception was raised

    def shouldTurnDeviceOff(self):
        print("shouldTurnDeviceOff")
        device = TelldusDevice("TV", 1, 1668440)
        device.turnoff()
        # ok if no exception was raised

if __name__ == '__main__':
    unittest.main()
